<div>
    <h1>Prueba técnica en ARKON DATA</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](https://www.andreemalerva.com/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/test-arkon), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este es un proyecto realizado con PHP MAIL, html, css, javascript y JQUERY.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://test-arkon.netlify.app/)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2023 LYAM ANDREE CRUZ MALERVA
