$('#formArkon button').click(function (e) {
    e.preventDefault();
    var form = $('#formArkon');
    sendEmail(form);
});

function sendEmail(form) {
    const namePerson = document.getElementById('nameData').value;
    const lastName = document.getElementById('lastName').value;
    const company = document.getElementById('companyName').value;
    const email = document.getElementById('email').value;

    let dataObjs = {
        name: namePerson,
        lastName: lastName,
        company: company,
        email: email
    }
    const url = "send.php";
    const post = JSON.stringify(dataObjs)

    var request = $.ajax({
        url: url,
        method: 'POST',
        type:'POST',
        contentType:'application/json; charset=utf-8',
        data:  post,
        dataType: 'JSON',
        timeout: 1000, 
        success: function (r) {
            console.log(r);
            console.log('Information SUCCESS:', post);
        },
        error: function (xhr) {
            console.log(xhr.status);
            console.log(xhr.responseText);
            console.log('Information FAIL:', post);
        },
    })

    request.done(function (msg) {
        console.log('Ready!', msg);
    });

    request.fail(function (jqXHR, textStatus) {
        alert("Request failed: " + textStatus);
    });
}